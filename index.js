#!/usr/bin/env node
'use strict';
var argv = require('optimist')
    .demand(['m', 'c'])
    .alias('m', 'manifest')
    .alias('c', 'config')
    .describe('m', 'Path to manifest file')
    .describe('c', 'Path to configuration file')
    .argv;

var fs = require('fs');

var Client = require('dwc-client');

var dispatcher = require('./lib/dispatcher');

var readJSON = function(path) {
    if (!fs.existsSync(path)) {
        throw new Error('File in path [' + path + '] not found');
    }

    var contents = fs.readFileSync(path, {encoding: 'utf8'}),
        parsedContents;

    try {
        parsedContents = JSON.parse(contents);
    } catch (err) {
        throw new Error('Failed to parse file, ' + err.message);
    }

    return parsedContents;
};

var config = readJSON(argv.c),
    manifest = readJSON(argv.m);

var client = new Client({
    amqp: config.queue
});

client.init(function() {
    dispatcher(config.dispatcher, manifest, client);
});

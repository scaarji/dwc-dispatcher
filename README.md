# Dispatcher

This package contains service responsible for handling HTTP requests and transforming them into requests to services via the bus.

## Configuration

Dispatcher accept configuration object that should have:

- `queue` section with configuration of `dwc-client`, information can be found in corresponding [documentation](https://bitbucket.org/scaarji/dwc-client)
- `dispatcher` section with configuration of dispatcher itself, it should contain:
    - `port` the port on which dispatcher should start
    - `timeout` timeout for requests via client
    - `auth` information about service that should be used to validate tokens, it should contain information:
        - `service` information about service (`name` and `version`)
        - `op` name of the operation that can be used to get token info

## Manifest

Manifest is a file that contains bindings of HTTP requests to bus requests, it has to be a JSON file that contains array of routes, each one contains fields:

- `method` - HTTP request method
- `path` - url to match
- `service` - name of the service
- `op` - operation to be mapped to this request

## Usage

Create `config.json` and `manifest.json` files. Start service via:

    ~/index.js -c /path/to/config.json -m /path/to/manifest.json

## To Be Done

- Validate passed configuration
- Add logging
- Extract error code from error message
- Add tests
- Add authorized routes
- Add performance tests
- Add service version extraction from request headers

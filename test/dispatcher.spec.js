/* global describe, it */
describe('Dispatcher', function() {
    it('should make request to get token when passed via query parameter');
    it('should make request to get token when passed via header parameter');
    it('should not make request to get token if it is not given');
    it('should correctly pass request to service and return success response');
    it('should correctly pass request to service and return error response');
    it('should pass all paramters (body, params, query) to service');
    it('should correctly select service version based on request');
});

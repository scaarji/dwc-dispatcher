
'use strict';
var _ = require('lodash'),
    express = require('express'),
    bodyParser = require('body-parser');

module.exports = function(config, manifest, client) {
    var createHandler = function(item) {
        return function(req, res) {
            var message = {
                service: {
                    name: item.service,
                    version: 1
                },
                http: {
                    method: req.method,
                    path: req.path,
                    query: req.query,
                    params: req.params,
                    body: req.body
                },
                op: item.op,
                params: _.extend({}, req.query, req.params, req.body),
                user: req.user
            };

            client.query(message, function(err, response) {
                if (err) {
                    res.status(500).send(err.message);
                } else {
                    res.send(JSON.stringify(response));
                }
            });
        };
    };

    var extractToken = function(req) {
        var accessToken;
        if (!req.query.accessToken) {
            var header = req.get('authorization');
            if (header && /bearer /i.test(header)) {
                accessToken = header.split(' ').pop();
            }
        } else {
            accessToken = req.query.accessToken;
        }
        return accessToken;
    };

    var populateUser = function(req, res, next) {
        if (req.url === '/_health') {
            return process.nextTick(next);
        }
        var accessToken = extractToken(req);

        if (!accessToken) {
            return next();
        }

        client.query(_.extend({
            params: {
                id: accessToken
            }
        }, config.auth), function(err, result) {
            if (err) {
                return next(err);
            }

            req.user = result;
            next();
        });
    };

    if (config.timeout) {
        client.timeout = config.timeout;
    }

    var app = express();
    app.use(bodyParser.json());
    app.use(populateUser);
    app.get('/identity', function(req, res) {
        if (req.user) {
            res.send(req.user);
        } else {
            res.status(401).send();
        }
    });
    app.get('/_health', function(req, res) {
        res.status(200).send();
    });
    _.each(manifest, function(item) {
        app[item.method](item.path, createHandler(item));
    });

    var server = app.listen(config.port, function () {
        var host = server.address().address;
        var port = server.address().port;

        console.log('App listening at http://%s:%s', host, port);
    });
};
